
import java.util.*;

// Classe para geração de números aleatórios segundos várias distribuições
// Apenas a distribuição exponencial negativa está definida
public class Aleatorio {
    // Gera um número segundo uma distribuição exponencial negativa de média m
    static double exponencial (double m, int seed){
        return -m*Math.log(RandomGenerator.rand64(seed));
    }
    //normal
    static List<Double> normal (double m, double dp, int seed){ 
        //Acetato Aula13 pg. 163
        double V1, V2;
        double W;
        double Y1, Y2;
        double X1, X2;
        List<Double> vectNormal = new ArrayList<Double>();
        
        do{
            do {
                V1 = 2*(RandomGenerator.rand64(seed)) - 1;
                V2 = 2*(RandomGenerator.rand64(seed)) - 1;
                W = V1*V1 + V2*V2;
            } while (W>1);
        
            Y1 = V1 * Math.sqrt(-2*Math.log(W)/W); //N(0,1)
            Y2 = V2 * Math.sqrt(-2*Math.log(W)/W);
        
            X1 = m + Y1*dp; //N(m,d)
            X2 = m + Y2*dp;
            
        } while(X1 < 0 || X2 < 0);
        
        vectNormal.add(X1);
        vectNormal.add(X2);
        
        return vectNormal;
    }
}
