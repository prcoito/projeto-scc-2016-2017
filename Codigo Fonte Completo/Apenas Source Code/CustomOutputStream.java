import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import javax.swing.JTextArea;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PauloCoito
 */
public class CustomOutputStream extends OutputStream {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    private JTextArea textArea;

    public CustomOutputStream(JTextArea textArea) {
        this.textArea = textArea;
    }
    public void flush() {
        //textArea.repaint();
        try {
        textArea.append(buffer.toString("utf-8"));
        buffer.reset();
        } catch (UnsupportedEncodingException e){}
    }
    @Override
    public void write(int b) throws IOException {
        // redirects data to the text area
        buffer.write(b);
        //textArea.append(String.valueOf((char)b));
        //textArea.append(new String(new byte[] {(byte)b}));
        // scrolls the text area to the end of data
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
}
    