//Classe que representa a chegada de um cliente. Deriva de Evento.

public class Chegada extends Evento {
    //Construtor
    Chegada (double i, Simulador s, Servico se){
        super (i, s, se);
    }

    // Método que executa as acções correspondentes à chegada de um cliente
    @Override
    void executa (){
        // Coloca cliente no serviço - na fila ou a ser atendido, conforme o caso
        this.servico.insereServico (new Cliente());
        // Agenda nova chegada para daqui a Aleatorio.exponencial(s.media_cheg) instantes
        s.insereEvento (new Chegada(s.getInstante()+Aleatorio.exponencial(servico.getMedia_cheg(), servico.getSeed()), s, this.servico));
    }

    // Método que descreve o evento.
    // Para ser usado na listagem da lista de eventos.
    @Override
    public String toString(){
         return "Chegada em " + instante;
    }
}