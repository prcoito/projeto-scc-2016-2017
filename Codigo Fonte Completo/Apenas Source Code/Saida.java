//Classe que representa a saída de um cliente. Deriva de Evento.

public class Saida extends Evento {

    //Construtor
    Saida (double i, Simulador s, Servico se){
        super (i, s,se);
    }

    // Método que executa as acções correspondentes à saída de um cliente
    @Override
    void executa (){
        // Retira cliente do serviço
        this.servico.removeServico();
     }

    // Método que descreve o evento.
    // Para ser usado na listagem da lista de eventos.
    @Override
    public String toString(){
        return "Saída em " + instante;
    }

}