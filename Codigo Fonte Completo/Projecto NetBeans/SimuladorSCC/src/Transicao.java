public class Transicao extends Evento{

    Transicao(double i, Simulador s, Servico serv){
        super(i,s, serv);
    }
    
    @Override
    void executa() {
        Servico proximo;
        // s.getArrayServicos() == [perf1, perf2, pol1, pol2, env]  
        Cliente c = servico.removeServico();
        
        int index = this.s.getArrayServicos().indexOf(this.servico);
           
        if(this.servico.getNomeServico().equals("Polidora 2")) {
            proximo = (Servico)this.s.getArrayServicos().get(index +1);
            proximo.insereServico(c);
        }
        else {
            proximo = (Servico)this.s.getArrayServicos().get(index +2);
            proximo.insereServico(c);
        }
    }
    
      @Override
    public String toString(){
        return "Transicao em " + instante;
    }
    
}
