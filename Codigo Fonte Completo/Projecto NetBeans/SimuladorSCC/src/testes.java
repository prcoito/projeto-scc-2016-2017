
public class testes {
    public static void main(String[] args){
        //double tempoSim = 480;
        int numAtendPerf1 = 1,numAtendPerf2 = 1, numAtendPol1 = 1,numAtendPol2 = 3, numAtendEnv = 2;
        double medChegadaPerf1 = 5, mediaAtendPerf1 = 2, desvioAtendPerf1 = 0.7;
        double medChegadaPerf2 = 1.33, mediaAtendPerf2 = 0.75, desvioAtendPerf2 = 0.3;
        double mediaAtendPol1 = 4, desvioAtendPol1 = 1.2;
        double mediaAtendPol2 = 3, desvioAtendPol2 = 1;
        double mediaAtendEnv = 1.4, desvioAtendEnv = 0.3;
        Simulador s;
        for(int k=0;k<3;k++){
        System.out.println("Par. Mudado\tServiço:\tTempo médio de espera\tComp. médio da fila\tUtilização do serviço\tTempo de simulação\tNúmero de clientes atendidos\tNúmero de clientes na fila");
        
        /*****************************************************************************************************************************************/
        /*                                           VARIACAO DE TEMPO SIM E NUM ATENDEDORES                                                     */
        /*****************************************************************************************************************************************/
        /*for(double i = tempoSim/8; i<=50*60;i+=60){
            s = new Simulador(i, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            
            System.out.println("tempoSim = "+i);
            s.executa();
            
            //System.out.println("*************************************************************************************************************");
        }
        
         */   //System.out.println("*************************************************************************************************************");
        for(int i = numAtendPerf1; i<=numAtendPerf1+4; i+=1){
            System.out.println("numAtendPerf1 = "+i);
            s = new Simulador(1440, i, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(int i = numAtendPerf2; i<=numAtendPerf2+4; i+=1){
            System.out.println("numAtendPerf2 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        i, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(int i = numAtendPol1; i<=numAtendPol1+4; i+=1){
            System.out.println("numAtendPol1 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        i, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(int i = numAtendPol2; i<=numAtendPol2+4; i+=1){
            System.out.println("numAtendPol2 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        i, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(int i = numAtendEnv-1; i<=numAtendEnv+3; i+=1){
            System.out.println("numAtendEnv = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        i, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
    
        /*****************************************************************************************************************************************/
        /*                                      VARIACAO DE MEDIAS DE CHEGADA, ATENDIMENTO E DESVIO                                              */
        /*****************************************************************************************************************************************/
        
        for(double i = medChegadaPerf1-2*(medChegadaPerf1*0.1); i<=medChegadaPerf1+2*(medChegadaPerf1*0.1); i+=(medChegadaPerf1*0.1)){
            System.out.println("medChegadaPerf1 = "+i);
            s = new Simulador(1440, numAtendPerf1, i, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = mediaAtendPerf1-2*(mediaAtendPerf1*0.1); i<=mediaAtendPerf1+3*(mediaAtendPerf1*0.1); i+=(mediaAtendPerf1*0.1)){
            System.out.println("mediaAtendPerf1 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, i , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = desvioAtendPerf1-2*(desvioAtendPerf1*0.1); i<=desvioAtendPerf1+2*(desvioAtendPerf1*0.1); i+=(desvioAtendPerf1*0.1)){
            System.out.println("desvioAtendPerf1 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , i,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = medChegadaPerf2-2*(medChegadaPerf2*0.1); i<=medChegadaPerf2+2*(medChegadaPerf2*0.1); i+=(medChegadaPerf2*0.1)){
            System.out.println("medChegadaPerf2 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, i, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = mediaAtendPerf2-2*(mediaAtendPerf2*0.1); i<=mediaAtendPerf2+2*(mediaAtendPerf2*0.1); i+=(mediaAtendPerf2*0.1)){
            System.out.println("mediaAtendPerf2 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, i, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = desvioAtendPerf2-2*(desvioAtendPerf2*0.1); i<=desvioAtendPerf2+3*(desvioAtendPerf2*0.1); i+=(desvioAtendPerf2*0.1)){
            System.out.println("desvioAtendPerf2 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, i,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = mediaAtendPol1-2*(mediaAtendPol1*0.1); i<=mediaAtendPol1+3*(mediaAtendPol1*0.1); i+=(mediaAtendPol1*0.1)){
            System.out.println("mediaAtendPol1 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, i, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = desvioAtendPol1-2*(desvioAtendPol1*0.1); i<=desvioAtendPol1+3*(desvioAtendPol1*0.1); i+=(desvioAtendPol1*0.1)){
            System.out.println("desvioAtendPol1 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, i,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = mediaAtendPol2-2*(mediaAtendPol2*0.1); i<=mediaAtendPol2+2*(mediaAtendPol2*0.1); i+=(mediaAtendPol2*0.1)){
            System.out.println("mediaAtendPol2 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, i, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = desvioAtendPol2-2*(desvioAtendPol2*0.1); i<=desvioAtendPol2+3*(desvioAtendPol2*0.1); i+=(desvioAtendPol2*0.1)){
            System.out.println("desvioAtendPol2 = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, i,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = mediaAtendEnv-2*(mediaAtendEnv*0.1); i<=mediaAtendEnv+2*(mediaAtendEnv*0.1); i+=(mediaAtendEnv*0.1)){
            System.out.println("mediaAtendEnv = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, i, desvioAtendEnv);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        for(double i = desvioAtendEnv-2*(desvioAtendEnv*0.1); i<=desvioAtendEnv+3*(desvioAtendEnv*0.1); i+=(desvioAtendEnv*0.1)){
            System.out.println("desvioAtendEnv = "+i);
            s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, i);
            s.executa();
            //System.out.println("*************************************************************************************************************");
        }
        
        
        
        /**for(int k=0;k<3;k++){
            System.out.println("Mes\tUtilização do serviço\tNúmero de clientes atendidos");
        for(int i =(8*60*20) ; i<=36*(8*60*20);i+=(8*60*20)){
            System.out.print(i/(8*60*20));
            s = new Simulador(i, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
            
        }*/
        //for(int i =(8*60*20) ; i<=1*(8*60*20);i+=(8*60*20)){
            //System.out.print((8*60*20)i/(8*60*20));
        
        /*for(int k=0;k<3;k++){
        System.out.println("\tServiço:\tTempo médio de espera\tComp. médio da fila\tUtilização do serviço\tTempo de simulação\tNúmero de clientes atendidos\tNúmero de clientes na fila");

        int numAtendPerf1 = 1,numAtendPerf2 = 1, numAtendPol1 = 1,numAtendPol2 = 2, numAtendEnv = 2;
        double medChegadaPerf1 = 5, mediaAtendPerf1 = 2, desvioAtendPerf1 = 0.7;
        double medChegadaPerf2 = 1.33, mediaAtendPerf2 = 0.75, desvioAtendPerf2 = 0.3;
        double mediaAtendPol1 = 4, desvioAtendPol1 = 1.2;
        double mediaAtendPol2 = 3, desvioAtendPol2 = 1;
        double mediaAtendEnv = 1.4, desvioAtendEnv = 0.3;
        Simulador s;

        s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
        System.out.println("*********************************************************");
        numAtendPol2 = 3;
        s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
        System.out.println("*********************************************************");
        numAtendPol2 = 2;
        mediaAtendPol2 = 1.7;
        s = new Simulador(1440, numAtendPerf1, medChegadaPerf1, mediaAtendPerf1 , desvioAtendPerf1,
                                        numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2,
                                        numAtendPol1, mediaAtendPol1, desvioAtendPol1,
                                        numAtendPol2, mediaAtendPol2, desvioAtendPol2,
                                        numAtendEnv, mediaAtendEnv, desvioAtendEnv);
            s.executa();
        System.out.println("*********************************************************");*/
    }}
    
}
