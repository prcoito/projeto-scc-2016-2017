
import java.util.*;
import javax.swing.JTextArea;

public class Simulador {

    // Relógio de simulação - variável que contém o valor do tempo em cada instante
    private double instante;
    //tempo simulaçao
    private double tempo_simulacao;
    // Serviço - pode haver mais do que um num simulador
    private Servico perf1;
    private Servico pol1;
    private Servico perf2;
    private Servico pol2;
    private Servico env;
    private ArrayList<Servico> arrayServicos;
    // Lista de eventos - onde ficam registados todos os eventos que vão ocorrer na simulação
    // Cada simulador só tem uma
    private ListaEventos lista;
    
    // Construtor
    public Simulador(double tempoSim, int numAtendPerf1, double medChegadaPerf1, double mediaAtendPerf1 , double desvioAtendPerf1,
                                      int numAtendPerf2, double medChegadaPerf2, double mediaAtendPerf2, double desvioAtendPerf2,
                                      int numAtendPol1, double mediaAtendPol1, double desvioAtendPol1,
                                      int numAtendPol2, double mediaAtendPol2, double desvioAtendPol2,
                                      int numAtendEnv, double mediaAtendEnv, double desvioAtendEnv) {
		// Inicialização de parâmetros do simulador
        tempo_simulacao = tempoSim;
	// Inicialização do relógio de simulação
	instante = 0;
	// Criação do serviço
        arrayServicos = new ArrayList();
	perf1 = new Servico (this, "Perforadora 1", numAtendPerf1, medChegadaPerf1, mediaAtendPerf1, desvioAtendPerf1, 1);
        perf2 = new Servico (this, "Perforadora 2", numAtendPerf2, medChegadaPerf2, mediaAtendPerf2, desvioAtendPerf2, 10);
        pol1 = new Servico (this, "Polidora 1", numAtendPol1, 0, mediaAtendPol1, desvioAtendPol1, 20);
        pol2 = new Servico (this, "Polidora 2", numAtendPol2, 0, mediaAtendPol2, desvioAtendPol2, 33);
        env = new Servico (this, "Envernizadora", numAtendEnv, 0, mediaAtendEnv, desvioAtendEnv, 49);
        arrayServicos.add(perf1);
        arrayServicos.add(perf2);
        arrayServicos.add(pol1);
        arrayServicos.add(pol2);
        arrayServicos.add(env);
        
	// Criação da lista de eventos
	lista = new ListaEventos(this);
	// Agendamento da primeira chegada
        // Se não for feito, o simulador não tem eventos para simular
	insereEvento(new Chegada(instante, this, perf1));
        insereEvento(new Chegada(instante, this, perf2));
    }

    // Método que insere o evento e1 na lista de eventos
    void insereEvento (Evento e1){
        lista.insereEvento (e1);
    }
    public Evento getNextSaida(){
        return lista.getFirstSaida();
    }
    // Método que actualiza os valores estatísticos do simulador
    private void act_stats(){
        perf1.act_stats();
        pol1.act_stats();
        perf2.act_stats();
        pol2.act_stats();
        env.act_stats();
    }

    // Método que apresenta os resultados de simulação finais
    private void relat(){
        
        System.out.println("\n------- Resultados finais -------\n");
        perf1.relat();
        pol1.relat();
        perf2.relat();
        pol2.relat();
        env.relat();
    }

    // Método executivo do simulador
    public void executa (){
        Evento e1;
        // Enquanto não atender todos os clientes
        //while (servico.getAtendidos() < n_clientes){
        while (instante < tempo_simulacao){
           // lista.print();  // Mostra lista de eventos - desnecessário; é apenas informativo
            e1 = (Evento)(lista.removeFirst());  // Retira primeiro evento (é o mais iminente) da lista de eventos
           // System.out.println(e1.servico + " "+ e1.getClass());
            instante = e1.getInstante();         // Actualiza relógio de simulação
            //System.out.println(instante);
            act_stats();                         // Actualiza valores estatísticos
            e1.executa(); // Executa evento
        }
        relat();  // Apresenta resultados de simulação finais
        
    }
    // Método que devolve o instante de simulação corrente
    public double getInstante() {
        return instante;
    }
    public ArrayList getArrayServicos(){
        return arrayServicos;
    }
}